@interface SBLockScreenNotificationListView : UIView
@end

@interface _SBFVibrantTableViewCell : UITableViewCell
@end

@interface SBNotificationCell : _SBFVibrantTableViewCell
@property(copy, nonatomic) NSString *secondaryText;
@property(copy, nonatomic) NSString *subtitleText;
@property(copy, nonatomic) NSString *primaryText;
@end

@interface SBLockScreenNotificationCell : SBNotificationCell
@property(readonly, retain, nonatomic) UIScrollView *contentScrollView; //SBLockScreenNotificationScrollView
@property(retain, nonatomic) id <UIScrollViewDelegate> delegate; //SBLockScreenNotificationListView
@end

@interface SBNotificationsBulletinCell : SBNotificationCell
@end

@interface NCNotificationContentView : UIView
@property (nonatomic,retain) NSString *primaryText;
@property (nonatomic,retain) NSString *secondaryText;
@end

@interface NCNotificationShortLookView : UIView
@property (getter=_notificationContentView,nonatomic,readonly) NCNotificationContentView *notificationContentView;
- (void)removeAllGestureRecognizers;
@end

@interface NCNotificationShortLookViewController : UIViewController <UIGestureRecognizerDelegate>
@property (nonatomic,readonly) NCNotificationShortLookView *viewForPreview;
@end

@interface NCNotificationLongLookView : UIView <UIGestureRecognizerDelegate> {
  NCNotificationContentView *_notificationContentView;
}
@property (nonatomic,readonly) UITapGestureRecognizer *lookViewTapGestureRecognizer;
@end

@interface UIInterfaceAction : NSObject
+ (id)actionWithTitle:(id)arg1 type:(long long)arg2 handler:(/*^block*/id)arg3;
@end

@interface NCNotificationRequestContentProvider
@property (nonatomic,copy,readonly) NSString *primaryText;
@property (nonatomic,copy,readonly) NSString *primarySubtitleText;
@property (nonatomic,copy,readonly) NSString *secondaryText;
@property (nonatomic,copy,readonly) NSString *summaryText;
@property (nonatomic,copy,readonly) id cancelAction; // cancel out of long look view
@property (nonatomic,copy,readonly) id clearAction;
@property (nonatomic,copy,readonly) id closeAction; // remove notification
@property (nonatomic,copy,readonly) id defaultAction;
- (id)_appBundleIdentifer;
@end