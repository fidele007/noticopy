#import "Headers.h"

#define STEAM_BUNDLE_ID @"com.valvesoftware.Steam"
#define allTrim(object) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]

static void ShowCopiedToClipboard(UIView *notiView) {
  UIView *customView = [[UIView alloc] init];
  [customView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.9]];
  customView.layer.cornerRadius = 10;
  customView.userInteractionEnabled = NO;

  UILabel *label = [[UILabel alloc] init];
  [label setText:@"Copied to clipboard."];
  [label setTextAlignment:NSTextAlignmentCenter];
  [label setTextColor:[UIColor whiteColor]];
  [customView addSubview:label];
  label.translatesAutoresizingMaskIntoConstraints = false;
  [label.centerXAnchor constraintEqualToAnchor:customView.centerXAnchor].active = true;
  [label.centerYAnchor constraintEqualToAnchor:customView.centerYAnchor].active = true;

  customView.alpha = 0;
  [notiView.superview addSubview:customView];

  customView.translatesAutoresizingMaskIntoConstraints = false;
  [customView.centerXAnchor constraintEqualToAnchor:notiView.centerXAnchor].active = true;
  [customView.centerYAnchor constraintEqualToAnchor:notiView.centerYAnchor].active = true;

  [customView.widthAnchor constraintEqualToAnchor:label.widthAnchor constant:10].active = true;
  [customView.heightAnchor constraintEqualToAnchor:label.heightAnchor constant:10].active = true;

  [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationCurveEaseOut
    animations:^{
      customView.alpha = 1;
    }
    completion:^(BOOL finished){
      [UIView animateWithDuration:0.3 delay:0.5 options:UIViewAnimationCurveEaseOut
        animations:^{
          customView.alpha = 0;
        }
        completion:^(BOOL finished) {
          [customView removeFromSuperview];
        }];
    }];
}

%group iOS12Down
// Hook notification cells on lock screen
%hook SBLockScreenNotificationCell
- (id)initWithStyle:(long long)arg1 reuseIdentifier:(id)arg2 {

  UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]
    initWithTarget:self action:@selector(handleNotificationLongPress:)];

  [self addGestureRecognizer:longPressGestureRecognizer];

  return %orig;
}

%new
- (void)handleNotificationLongPress:(UILongPressGestureRecognizer *)gesture {
  if (gesture.state == UIGestureRecognizerStateEnded) {
    // Copy text to pasteboard
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.secondaryText;

    // Add alert view to let users know the text is copied
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    CGRect customViewFrame = CGRectMake(screenSize.width/2 - 80, screenSize.height/2, 160, 40);
    UIView *customView = [[UIView alloc] initWithFrame:customViewFrame];
    [customView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.8]];
    customView.layer.cornerRadius = 10;
    customView.userInteractionEnabled = NO;

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160, 40)];
    [label setText:@"Copied to clipboard."];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor whiteColor]];
    [customView addSubview:label];

    customView.alpha = 0;
    [self.window.rootViewController.view addSubview:customView];

    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationCurveEaseOut
      animations:^{
        customView.alpha = 1;
      }
      completion:^(BOOL finished){
        [UIView animateWithDuration:0.3 delay:0.5 options:UIViewAnimationCurveEaseOut
          animations:^{
            customView.alpha = 0;
          }
          completion:^(BOOL finished) {
            [customView removeFromSuperview];
          }];
      }];
  }
}
%end


// Hook notification cells in Notification Center
%hook SBNotificationsBulletinCell
- (id)initWithStyle:(long long)arg1 reuseIdentifier:(id)arg2 {
  UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]
    initWithTarget:self action:@selector(handleNotificationLongPress:)];

  // longPressGestureRecognizer.minimumPressDuration = 1.0;
  [self addGestureRecognizer:longPressGestureRecognizer];

  return %orig;
}

%new
- (void)handleNotificationLongPress:(UILongPressGestureRecognizer *)gesture {
  if (gesture.state == UIGestureRecognizerStateBegan) {
    // Copy text to pasteboard
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.secondaryText;

    // Add alert view to let users know the text is copied
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    CGFloat customViewX = screenSize.width/2 - 80;
    CGFloat customViewY = self.frame.origin.y + self.frame.size.height/2 - 20;
    CGRect customViewFrame = CGRectMake(customViewX, customViewY, 160, 40);
    UIView *customView = [[UIView alloc] initWithFrame:customViewFrame];
    [customView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.8]];
    customView.layer.cornerRadius = 10;
    customView.userInteractionEnabled = NO;

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160, 40)];
    [label setText:@"Copied to clipboard."];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor whiteColor]];
    [customView addSubview:label];

    customView.alpha = 0;
    [self.superview addSubview:customView];

    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationCurveEaseOut
      animations:^{
        customView.alpha = 1;
      }
      completion:^(BOOL finished){
        [UIView animateWithDuration:0.3 delay:0.5 options:UIViewAnimationCurveEaseOut
          animations:^{
            customView.alpha = 0;
          }
          completion:^(BOOL finished) {
            [customView removeFromSuperview];
          }];
      }];
  }
}
%end

// iOS 11
%hook NCNotificationContentView
- (id)initWithStyle:(long long)arg1 {
  UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]
    initWithTarget:self action:@selector(handleNotificationLongPress:)];

  [self addGestureRecognizer:longPressGestureRecognizer];

  return %orig;
}

%new
- (void)handleNotificationLongPress:(UILongPressGestureRecognizer *)gesture {
  if (gesture.state != UIGestureRecognizerStateBegan) return;

  NSString *copiedText = @"";
  if (self.primaryText && self.primaryText.length > 0) {
    copiedText = [self.primaryText copy];
  }
  if (self.secondaryText && self.secondaryText.length > 0) {
    copiedText = [NSString stringWithFormat:@"%@\n%@", copiedText, self.secondaryText];
  }

  UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
  pasteboard.string = copiedText;

  ShowCopiedToClipboard(self);
}
%end
%end

static NSString *GetSteamCodeFromText(NSString *text) {
  if ([text length] == 0) return nil;

  NSString *code = @"";
  NSRange searchRange = NSMakeRange(0, [text length]);
  NSString *codePattern = @"code ([^\\s]+)";
  NSError  *error = nil;
  NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:codePattern
                                                                         options:0
                                                                           error:&error];
  NSArray *matches = [regex matchesInString:text options:0 range:searchRange];
  for (NSTextCheckingResult *match in matches) {
    NSString *matchedString = [text substringWithRange:[match rangeAtIndex:1]];
    code = [code stringByAppendingString:matchedString];
  }

  return code;
}

static NSString *GetCodeFromText(NSString *text) {
  if ([text length] == 0) return nil;

  NSString *code = @"";
  NSRange searchRange = NSMakeRange(0, [text length]);
  NSString *codePattern = @"\\d{3,}";  // Detect number of at least 3 digits
  NSError  *error = nil;
  NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:codePattern
                                                                         options:0
                                                                           error:&error];
  NSArray *matches = [regex matchesInString:text options:0 range:searchRange];
  for (NSTextCheckingResult *match in matches) {
    NSString *matchedString = [text substringWithRange:[match range]];
    code = [code stringByAppendingString:matchedString];
  }

  return code;
}

%group iOS13Up
%hook NCNotificationRequestContentProvider
- (NSArray *)interfaceActions {
  // NSLog(@"interfaceActions: %@", %orig);
  NSMutableArray *interfaceActions;
  if (%orig) {
    interfaceActions = [%orig mutableCopy];
  } else {
    interfaceActions = [NSMutableArray array];
  }
  
  UIInterfaceAction *copyTextAction = [%c(UIInterfaceAction) actionWithTitle:@"Copy text"
                                                                        type:0
                                                                     handler:^{
    NSString *copiedText = @"";
    if ([allTrim(self.primaryText) length] > 0) {
      copiedText = [NSString stringWithFormat:@"%@\n", self.primaryText];
    }

    if ([allTrim(self.secondaryText) length] > 0) {
      copiedText = [NSString stringWithFormat:@"%@%@", copiedText, self.secondaryText];
    }

    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = copiedText;

    [self.cancelAction invoke];
  }];
  
  [interfaceActions addObject:copyTextAction];

  NSString *appID = [self _appBundleIdentifer];
  // NSLog(@"[NotiCopy] appID: %@", appID);
  NSString *code = @"";
  if ([appID isEqualToString:STEAM_BUNDLE_ID]) {
    code = GetSteamCodeFromText(self.secondaryText);
  } else {
    code = GetCodeFromText(self.secondaryText);
  }

  // NSLog(@"[NotiCopy] code: %@", code);

  if ([code length] > 0) {
    UIInterfaceAction *copyCodeAction = [%c(UIInterfaceAction) actionWithTitle:@"Copy code"
                                                                        type:0
                                                                     handler:^{
      [UIPasteboard generalPasteboard].string = code;

      [self.cancelAction invoke];
    }];

    [interfaceActions addObject:copyCodeAction];
  }

  return interfaceActions;
}
%end
%end

%ctor {
  if (kCFCoreFoundationVersionNumber >= 1665.15) {
    %init(iOS13Up);
  } else {
    %init(iOS12Down);
  }
}