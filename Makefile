ARCHS = armv7 armv7s arm64 arm64e

include $(THEOS)/makefiles/common.mk

TWEAK_NAME = NotiCopy
NotiCopy_FILES = Tweak.xm
NotiCopy_FRAMEWORKS = UIKit
NotiCopy_CFLAGS = -fobjc-arc

include $(THEOS_MAKE_PATH)/tweak.mk

after-install::
	install.exec "killall -9 backboardd"
